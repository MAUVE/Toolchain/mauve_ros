/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE ROS project.
 *
 * MAUVE ROS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE ROS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include <mauve/runtime.hpp>
#include <mauve/ros.hpp>
#include <std_msgs/String.h>

#include <mauve/ros/conversions.hpp>

using namespace mauve::runtime;
using namespace mauve::ros;

struct MyShell : public Shell {
  ReadPort<StatusValue<std::string>> & data =
    mk_read_port< StatusValue<std::string> >("data", {DataStatus::NO_DATA, ""} );
};

struct MyCore : public Core<MyShell> {
  void update() {
    auto sv = shell().data.read();
    this->logger().info("received {} ({})", sv.value, sv.status);
  }
};

using MyComponent = Component<MyShell, MyCore, PeriodicStateMachine<MyShell, MyCore> >;

struct TestArchitecture : public Architecture {
  using sub_t = SubscriberResource<std_msgs::String, std::string>;

  sub_t & sub_convert = mk_resource<sub_t>("sub_convert");
  sub_t & sub_conversion = mk_resource<sub_t>("sub_conversion", "/my_str");
  sub_t & sub_construct = mk_resource<sub_t>("sub_construct", "/my_str",
    conversions::convert<std_msgs::String,std::string>);

  MyComponent & c1 = mk_component<MyComponent>("cpt_convert");
  MyComponent & c2 = mk_component<MyComponent>("cpt_conversion");
  MyComponent & c3 = mk_component<MyComponent>("cpt_construct");

  SubscriberComponent<std_msgs::String, std::string> & sub_comp =
    mk_component<SubscriberComponent<std_msgs::String, std::string>>("subscriber",
      conversions::convert<std_msgs::String,std::string>
    );

  virtual bool configure_hook() override {
    c1.shell().data.connect(sub_convert.interface().read);
    c2.shell().data.connect(sub_conversion.interface().read);
    c3.shell().data.connect(sub_construct.interface().read);

    sub_convert.shell().topic = "/my_str";
    sub_convert.shell().conversion.set_value([this](const std_msgs::String& msg, std::string& data) -> bool {
      if (msg.data == "hello") {
        data = "world";
        return true;
      } else return false;
    });
    sub_conversion.shell().conversion.set_value(conversions::convert<std_msgs::String,std::string>);

    sub_comp.shell().topic = "/my_str";
    sub_comp.fsm().period = ms_to_ns(100);

    return Architecture::configure_hook();
  }
};

int main() {
  std::stringstream config;
  config << "default:" << std::endl;
  config << "  type: stdout" << std::endl;
  config << "  level: debug" << std::endl;
  AbstractLogger::initialize(config);

  auto archi = new TestArchitecture();
  auto depl = mk_deployer(archi);
  archi->configure();
  depl->create_tasks();
  depl->activate();
  depl->start();
  depl->loop();
  depl->stop();
  archi->cleanup();
  return 0;
}
