/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE ROS project.
 *
 * MAUVE ROS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE ROS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include <mauve/runtime.hpp>
#include <mauve/ros.hpp>
#include <geometry_msgs/PoseStamped.h>
#include <mauve/runtime/PeriodicStateMachine.hpp>
#include <mauve/types/geometry/Pose2D.hpp>
#include <mauve/ros/conversions.hpp>

using namespace mauve::runtime;
using namespace mauve::ros;
using namespace mauve::types::geometry;

struct ProdShell : public Shell {
  WritePort<Pose2D> & data = mk_write_port<Pose2D>("data");
};

struct ProdCore : public Core<ProdShell> {
  void update() {
    Pose2D p { static_cast<double>(rand()), static_cast<double>(rand()), static_cast<double>(rand()) };
    shell().data.write(p);
  }
};

using Producer = Component<ProdShell, ProdCore, PeriodicStateMachine<ProdShell, ProdCore> >;

bool my_convert(const Pose2D& data, geometry_msgs::PoseStamped& msg, std::string frame) {
  time_ns_t t = AbstractDeployer::instance()->now();
  struct timespec ts;
  ns_to_timespec(t, &ts);
  msg.header.stamp = ros::Time(ts.tv_sec, ts.tv_nsec);
  msg.header.frame_id = frame;
  return conversions::convert(data, msg.pose);
};

struct TestArchitecture : public Architecture {
  Producer & prod = mk_component<Producer>("producer");

  using pub_t = PublisherResource<Pose2D, geometry_msgs::PoseStamped>;

  pub_t & pub_lambda = mk_resource<pub_t>("pub_lambda", "/pose");
  pub_t & pub_bind = mk_resource<pub_t>("pub_bind", "/pose");

  virtual bool configure_hook() override {
    prod.shell().data.connect(pub_lambda.interface().write);
    prod.shell().data.connect(pub_bind.interface().write);

    pub_lambda.shell().conversion.set_value(
      [](const Pose2D& data, geometry_msgs::PoseStamped& msg) -> bool {
        time_ns_t t = AbstractDeployer::instance()->now();
        struct timespec ts;
        ns_to_timespec(t, &ts);
        msg.header.stamp = ros::Time(ts.tv_sec, ts.tv_nsec);
        msg.header.frame_id = "lambda";
        return conversions::convert(data, msg.pose);
    });
    pub_bind.shell().conversion.set_value(
      [](const Pose2D& data, geometry_msgs::PoseStamped& msg) -> bool {
        return my_convert(data, msg, "bind");
    });

    return Architecture::configure_hook();
  }
};

int main() {
  std::stringstream config;
  config << "default:" << std::endl;
  config << "  type: stdout" << std::endl;
  config << "  level: debug" << std::endl;
  AbstractLogger::initialize(config);

  auto archi = new TestArchitecture();
  auto depl = mk_deployer(archi);
  archi->configure();
  depl->create_tasks();
  depl->activate();
  depl->start();

  depl->loop();

  depl->stop();
  archi->cleanup();
  return 0;
}
