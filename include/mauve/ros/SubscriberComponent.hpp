/*
* Copyright 2017 ONERA
*
* This file is part of the MAUVE ROS project.
*
* MAUVE ROS is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License version 3 as
* published by the Free Software Foundation.
*
* MAUVE ROS is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
*/
#ifndef MAUVE_ROS_SUBSCRIBER_COMPONENT_HPP
#define MAUVE_ROS_SUBSCRIBER_COMPONENT_HPP

#include <string>
#include <functional>

#include <ros/ros.h>

#include <mauve/runtime.hpp>
#include <mauve/runtime/PeriodicStateMachine.hpp>

#include "ros.hpp"

namespace mauve {
  namespace ros {

    template <typename T, typename ROS_T>
    struct SubscriberComponentShell : public runtime::Shell {
      runtime::Property<std::string> & topic = this->template mk_property<std::string>("topic", "");
      runtime::WritePort<T> & port = mk_port< runtime::WritePort<T> >("port");
    };

    template <typename T, typename ROS_T>
    struct SubscriberComponentCore
    : public runtime::Core< SubscriberComponentShell<T, ROS_T> >
    , public Connector {

      virtual bool configure_hook() override {
        if (node == nullptr) {
          this->logger().info("Initializing ROS node");
          if (init_node("mauve", 1)) {
            node = new ::ros::NodeHandle();
            node_private = new ::ros::NodeHandle("~");
          }
          else {
            this->logger().error("Unable to initialize ROS node");
            return false;
          }
        }

        std::string & topic = this->shell().topic;
        if (topic.empty()) return false;
        if(topic.at(0) == '~')
        subscriber = node_private->subscribe(topic.substr(1), 10, &SubscriberComponentCore<T,ROS_T>::callback, this);
        else
        subscriber = node->subscribe(topic, 10, &SubscriberComponentCore<T,ROS_T>::callback, this);
        return true;
      }

      virtual void cleanup_hook() override {
        subscriber.shutdown();
      }

      virtual void update() {
        T value;
        mutex.lock();
        ::mauve::ros::convert(msg, value);
        mutex.unlock();
        this->shell().port.write(value);
      }

    private:
      /** The subscriber callback */
      void callback(const ROS_T& msg) {
        mutex.lock();
        this->msg = msg;
        mutex.unlock();
      }

      /** The ROS subscriber */
      ::ros::Subscriber subscriber;
      /** The value owned by the resource */
      ROS_T msg;
      /** A mutex to protect the \a value */
      runtime::RtMutex mutex;
    };

    template <typename T, typename ROS_T>
    using SubscriberComponent = runtime::Component<
    SubscriberComponentShell<T, ROS_T>,
    SubscriberComponentCore<T, ROS_T>,
    runtime::PeriodicStateMachine<SubscriberComponentShell<T, ROS_T>,
    SubscriberComponentCore<T, ROS_T> > >;

  }
}

#endif
