/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE ROS project.
 *
 * MAUVE ROS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE ROS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_ROS_CONNECTOR_HPP
#define MAUVE_ROS_CONNECTOR_HPP

#include <string>
#include <mauve/runtime.hpp>
#include <ros/ros.h>

namespace mauve {

  namespace ros {
    /** ROS Connector interface.
     * Manages connection and node handles.
     */
    struct Connector : virtual public runtime::WithLogger {
      /** Initialize ROS Node */
      bool init();
      /** An handle to the ROS node */
      static ::ros::NodeHandle* node;
      /** An handle to the private view of the ROS node */
      static ::ros::NodeHandle* node_private;
    };

  }
}

#endif
