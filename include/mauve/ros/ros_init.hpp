/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE ROS project.
 *
 * MAUVE ROS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE ROS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_ROS_INIT_HPP
#define MAUVE_ROS_INIT_HPP

#include <string>
#include <ros/ros.h>

namespace mauve {

  /**
   * The \a ros namespace contains the ROS binding resources
   */
  namespace ros {

    /** Initialize the deployment as a ROS node.
     * \param node_name The node name (made anonymous)
     * \param thread_count Number of threads of the spinner
     * \param remappings A map of names remapping
     * \return true if node initialized
     */
    bool init_node(const std::string& node_name, int thread_count = 1,
      const ::ros::M_string& remappings = ::ros::M_string());

    /** Initialize the deployment as a ROS node.
     * \param node_name The node name (made anonymous)
     * \param thread_count Number of threads of the spinner
     * \param argc number of command line arguments
     * \param argv command line arguments
     * \return true if node initialized
     */
    bool init_node(int& argc, char** argv, const std::string& node_name,
      int thread_count=1);

  }
}

#endif
