/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE ROS project.
 *
 * MAUVE ROS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE ROS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_ROS_SHELL_HPP
#define MAUVE_ROS_SHELL_HPP

#include <string>
#include <functional>
#include <ros/ros.h>
#include <mauve/runtime.hpp>
#include "Connector.hpp"

namespace mauve {

  namespace ros {

    /** ROS Abstract Shell. */
    struct RosAbstractShell
      : public runtime::Shell
      , virtual public Connector
    {
      /** Topic property */
      runtime::Property<std::string>& topic = mk_property<std::string>("topic", "");
      /** Configure shell: init ROS node */
      virtual bool configure_hook();
    };

    /** ROS Shell for conversion.
     * \tparam T MAUVE Type
     * \tparam ROS_T ROS Type
     */
    template <typename T, typename U>
    struct RosShell : public RosAbstractShell
    {
      /** Type of the conversion functions */
      using conversion_t = std::function<bool(const T&, U&)>;
      /** Conversion function property */
      runtime::Property<conversion_t>& conversion = mk_property<conversion_t>(
        "conversion", nullptr
        //[](const T&, U&) { return false; }
      );

      /** Read port for the publisher component */
      runtime::ReadPort<runtime::StatusValue<T>> & read_port =
        this->template mk_read_port<runtime::StatusValue<T>>("read",
          { runtime::DataStatus::NO_DATA, T() });
      /** Write port for the subscriber component */
      runtime::WritePort<U> & write_port =
        this->template mk_write_port<U>("write");

      /** Configure shell: init ROS node */
      virtual bool configure_hook() {
        if (this->conversion.get_value() == nullptr) {
          this->logger().error("conversion function not implemented");
          return false;
        }
        return RosAbstractShell::configure_hook();
      };

      /**
       * Default constructor.
       */
      RosShell() {};
      /**
       * Constructor with topic.
       * @param topic topic to subscribe to
       */
      RosShell(const std::string& topic) { this->topic = topic; };
      /**
       * Constructor with conversion.
       * @param convert conversion function
       */
      RosShell(const conversion_t& convert) { this->conversion.set_value(convert); };
      /**
       * Constructor with conversion and topic.
       * @param convert conversion function
       * @param topic topic to subscribe to
       */
      RosShell(const std::string& topic, const conversion_t& convert) {
         this->topic = topic; this->conversion.set_value(convert); };
    };

  }
}

#endif
