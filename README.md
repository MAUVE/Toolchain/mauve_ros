# MAUVE ROS

This package provides a ROS binding for MAUVE architectures. It provides
resources to publish or subscribe to topics, with conversion functions
from MAUVE types to ROS types.

MAUVE ROS is licensed under the
[GNU Lesser General Public License](https://www.gnu.org/licenses/lgpl-3.0).

## Documentation

The reference manual, the API documentation and some tutorials are available on https://mauve.gitlab.io
