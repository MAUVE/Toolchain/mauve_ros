/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE ROS project.
 *
 * MAUVE ROS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE ROS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include <tf/message_filter.h>

#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/MapMetaData.h>
#include <nav_msgs/OccupancyGrid.h>

#include <mauve/types/geometry_types.hpp>

#include "mauve/ros/conversions.hpp"

#include "ros/ros.h"

namespace mauve {
  namespace ros {
    namespace conversions {

template <> bool convert(const geometry_msgs::Pose2D& a, types::geometry::Pose2D& b)
{
  b.location.x = a.x;
  b.location.y = a.y;
  b.theta = a.theta;
  return true;
}

template <> bool convert(const types::geometry::Pose2D& a, geometry_msgs::Pose2D& b)
{
  b.x = a.location.x;
  b.y = a.location.y;
  b.theta = a.theta;
  return true;
}

template <> bool convert(const geometry_msgs::Pose2D& a, types::geometry::Point2D& b)
{
  b.x = a.x;
  b.y = a.y;
  return true;
}

template <> bool convert(const types::geometry::Point2D& a, geometry_msgs::Pose2D& b)
{
  b.x = a.x;
  b.y = a.y;
  b.theta = 0;
  return true;
}

template<> bool convert(const geometry_msgs::Point& a, types::geometry::Point2D& b)
{
  b.x = a.x;
  b.y = a.y;
  return true;
}

template<> bool convert(const types::geometry::Point2D& a, geometry_msgs::Point& b)
{
  b.x = a.x;
  b.y = a.y;
  return true;
}

template<> bool convert(const geometry_msgs::Pose& a, types::geometry::Pose2D& b)
{
  tf::Quaternion q;
  double roll, pitch, yaw;
  tf::quaternionMsgToTF(a.orientation, q);
  tf::Matrix3x3(q).getRPY(roll, pitch, yaw);
  b.theta = yaw;
  return convert(a.position, b.location);
}

template<> bool convert(const types::geometry::Pose2D& a, geometry_msgs::Pose& b)
{
  b.orientation = tf::createQuaternionMsgFromRollPitchYaw(0, 0, a.theta);
  return convert(a.location, b.position);
}

template<> bool convert(const geometry_msgs::PoseWithCovariance& a, types::geometry::Pose2D& b)
{
  return convert(a.pose, b);
}

template<> bool convert(const types::geometry::Pose2D& a, geometry_msgs::PoseWithCovariance& b)
{
  return convert(a, b.pose);
}

template<> bool convert(const geometry_msgs::PoseStamped& a, types::geometry::Pose2D& b)
{
  return convert(a.pose, b);
}

template<> bool convert(const types::geometry::Pose2D& a, geometry_msgs::PoseStamped& b)
{
  return convert(a, b.pose);
}

template<> bool convert(const geometry_msgs::PoseWithCovarianceStamped& a, types::geometry::Pose2D& b)
{
  return convert(a.pose, b);
}

template<> bool convert(const types::geometry::Pose2D& a, geometry_msgs::PoseWithCovarianceStamped& b)
{
  return convert(a, b.pose);
}

template<> bool convert(const types::geometry::UnicycleVelocity& a, geometry_msgs::Twist& b) {
  b.linear.x = a.linear;
  b.angular.z = a.angular;
  return true;
}

template<> bool convert(const geometry_msgs::Twist& a, types::geometry::UnicycleVelocity& b) {
  b.linear = a.linear.x;
  b.angular = a.angular.z;
  return true;
}

template<> bool convert(const nav_msgs::Odometry& a, types::geometry::Pose2D& b) {
  return convert(a.pose, b);
}

template<> bool convert(const types::geometry::Pose2D& a, nav_msgs::Odometry& b) {
  return convert(a, b.pose);
}

template<> bool convert(const nav_msgs::Odometry& a, types::geometry::UnicycleVelocity& b) {
  return convert(a.twist.twist, b);
}

template<> bool convert(const types::geometry::UnicycleVelocity& a, nav_msgs::Odometry& b) {
  return convert(a, b.twist.twist);
}

template<> bool convert(const types::geometry::Vector3& a, geometry_msgs::Vector3& b) {
  b.x = a.x;
  b.y = a.y;
  b.z = a.z;
  return true;
}

template<> bool convert(const geometry_msgs::Vector3& a, types::geometry::Vector3& b) {
  b.x = a.x;
  b.y = a.y;
  b.z = a.z;
  return true;
}

template<> bool convert(const types::geometry::Quaternion& a, geometry_msgs::Quaternion& b) {
  b.w = a.w;
  b.x = a.x;
  b.y = a.y;
  b.z = a.z;
  return true;
}

template<> bool convert(const geometry_msgs::Quaternion& a, types::geometry::Quaternion& b) {
  b.w = a.w;
  b.x = a.x;
  b.y = a.y;
  b.z = a.z;
  return true;
}

// need to sort after //

template<> bool convert(const types::geometry::Point& a, geometry_msgs::Point& b)
{
  b.x = a.x;
  b.y = a.y;
  b.z = a.z;
  return true;
}

template<> bool convert(const geometry_msgs::Point& a, types::geometry::Point& b)
{
  b.x = a.x;
  b.y = a.y;
  b.z = a.z;
  return true;
}

template<> bool convert(const types::geometry::Pose& a, geometry_msgs::Pose& b)
{
  return convert(a.position, b.position) && convert(a.orientation, b.orientation);
}

template<> bool convert(const geometry_msgs::Pose& a, types::geometry::Pose& b)
{
  return convert(a.position, b.position) && convert(a.orientation, b.orientation);
}

template<> bool convert(const types::geometry::Transform& a, geometry_msgs::Transform& b)
{
  return convert(a.translation, b.translation) && convert(a.rotation, b.rotation);
}

template<> bool convert(const geometry_msgs::Transform& a, types::geometry::Transform& b)
{
  return convert(a.translation, b.translation) && convert(a.rotation, b.rotation);
}

// remplir le Header ROS est il utile?

template<> bool convert(const types::geometry::PoseStamped& a, geometry_msgs::PoseStamped& b)
{
  return convert(a.pose, b.pose);
}

template<> bool convert(const geometry_msgs::PoseStamped& a, types::geometry::PoseStamped& b)
{
  return convert(a.pose, b.pose);
}

template<> bool convert(const types::geometry::TransformStamped& a, geometry_msgs::TransformStamped& b)
{

  return convert(a.transform, b.transform);
}

template<> bool convert(const geometry_msgs::TransformStamped& a, types::geometry::TransformStamped& b)
{
  return convert(a.transform, b.transform);
}

template<> bool convert(const types::geometry::MapMetaData& a, nav_msgs::MapMetaData& b)
{
  b.map_load_time = ::ros::Time(a.ts.tv_sec, a.ts.tv_nsec);
  b.resolution = a.resolution;
  b.width = a.width;
  b.height = a.height;
  return convert(a.origin, b.origin);
}

template<> bool convert(const nav_msgs::MapMetaData& a, types::geometry::MapMetaData& b)
{
  b.ts.tv_sec = a.map_load_time.sec;
  b.ts.tv_nsec = a.map_load_time.nsec;
  b.resolution = a.resolution;
  b.width = a.width;
  b.height = a.height;
  return convert(a.origin, b.origin);
}

template<> bool convert(const types::geometry::OccupancyGrid& a, nav_msgs::OccupancyGrid& b)
{
  b.header.stamp = ::ros::Time::now();
  b.data = std::vector<int8_t>(std::begin(a.data), std::end(a.data));
  return convert(a.info, b.info);
}

template<> bool convert(const nav_msgs::OccupancyGrid& a, types::geometry::OccupancyGrid& b)
{
  b.data = std::vector<int8_t>(std::begin(a.data), std::end(a.data));
  return convert(a.info, b.info);
}

    }
  }
}
